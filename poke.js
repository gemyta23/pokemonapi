     function consultPokemon(id, num) {
         fetch(`https://pokeapi.co/api/v2/pokemon/${id}`)
             .then(function (response) {
                 response.json()
                     .then(function (pokemon) {
                         createPokemon(pokemon, num)
                     })

             })

     }

     function consultPokemones() {
         let oneId = Math.round(Math.random() * 150)
         let twoId = Math.round(Math.random() * 150)

         consultPokemon(oneId, 1)
         consultPokemon(twoId, 2)

     }

     function createPokemon(pokemon, num) {
         //convertir la data en HTML
         let list = document.getElementById("list-Pokemon")
         let item = list.querySelector(`#pokemon-${num}`)

         let imagen = item.getElementsByTagName("img")[0]
         imagen.setAttribute("src", pokemon.sprites.front_default)

         let nombre = item.getElementsByTagName("p")[0]
         nombre.textContent = pokemon.name
     }

     consultPokemones()